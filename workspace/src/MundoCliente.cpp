// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fd2);
	close(fd3);
	unlink("/tmp/Pipe_Servidor-Cliente");
	unlink("/tmp/Pipe_Cliente-Servidor");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	int i;
	char cad[200];
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

//Recepción de datos del servidor
	if(read(fd2,cad,sizeof(cad))<0)
	{
		perror("read datos servidor");
		exit(1);
	}
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y,&esfera.radio, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	
// Actualizacion de los Datos compartidos
	
	dirdatos->esfera=esfera;
	dirdatos->raqueta1=jugador1;
	

	if( puntos1==3||puntos2==3)
	{
		dirdatos->fin=1;
		exit(0);
	}
	
	switch(dirdatos->accion)
	{
		case 1 :OnKeyboardDown('w',0,0);break;
		case -1 : OnKeyboardDown('s',0,0);break;
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	unsigned char tecla;
	
	switch(key)
	{
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;

	}
	tecla=key;
	if(write(fd3,&tecla,sizeof(tecla))<0)
	{
		perror("write tecla");
		exit(1);	
	}
}

void CMundo::Init()
{

//Memoria Compartida con Bot
	int fd_datos;
	void *map;
//Creacion del fichero
	fd_datos = open("/tmp/datos",O_CREAT|O_TRUNC|O_RDWR,0666);
	if (fd_datos < 0)
	{
		perror("open fd_datos");
		exit(1);
	}
//Establecimiento de la longitud del fichero
	if (ftruncate(fd_datos, sizeof(DatosMemCompartida))){
		perror("ftruncate");
		close(fd_datos);
		exit(1);
	}
//Proyección del fichero
	if((map=mmap(0, sizeof(DatosMemCompartida), PROT_WRITE|PROT_READ, MAP_SHARED, fd_datos,0))==MAP_FAILED){
		perror("mmap");
		close(fd_datos);
		exit(1);
	}
//Liberación del descriptor de fichero	
	if(close(fd_datos)<0)
	{	
		perror("close");
		exit(1);
	}
	dirdatos=(DatosMemCompartida*)map;
	
	dirdatos->fin=0;

//Creacion del escenario			
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
//FIFO de recepción de datos del servidor
	if(mkfifo("/tmp/Pipe_Servidor-Cliente", 0666)<0)
	{
		perror("mkfifo servidor-cliente");
		exit(1);
	}
	fd2=open("/tmp/Pipe_Servidor-Cliente",O_RDONLY);
	if(fd2<0)
	{
		perror("open fifo serv-cliente");
		exit(1);
	}
//FIFO de envio de datos al servidor
	if(mkfifo("/tmp/Pipe_Cliente-Servidor", 0666)<0)
	{
		perror("mkfifo cliente-servidor");
		exit(1);
	}
	fd3=open("/tmp/Pipe_Cliente-Servidor",O_WRONLY);
	if(fd3<0)
	{
		perror("open fifo cliente-servidor");
		exit(1);
	}

}
