#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <DatosMemCompartida.h>


int main (void){
	
	DatosMemCompartida *dirdatos;
	int fd;
	void* map;
	
	//Se abre el fichero que se quiere proyectar
	fd = open("/tmp/datos",O_RDWR);
	if (fd==-1){
	
		perror("open");
		return 1;
	}
	//Proyeccion del fichero en memoria
	if((map=mmap(0, sizeof(DatosMemCompartida), PROT_WRITE|PROT_READ, MAP_SHARED, fd,0))==MAP_FAILED)
	{
		perror("mmap");
		close(fd);
		return 1;
	}
	if(close(fd)==-1)
	{
		perror("close");
		return 1;
	}
	dirdatos=(DatosMemCompartida*)map;
	while(1)
	{	
		//Se cierra el programa al acabar la partida
		if(dirdatos->fin==1)
			return 0;
		//Eleccion de la accion a realizar segun la posicion de la raqueta y la pelota	
	 	if((dirdatos->esfera.centro.y)>(dirdatos->raqueta1.y2))
	 		dirdatos->accion=1;
	 	else if((dirdatos->esfera.centro.y)<(dirdatos->raqueta1.y1))
	 		dirdatos->accion=-1;
	 	else dirdatos->accion=0;
	
	usleep(25000);
	
	}



}

