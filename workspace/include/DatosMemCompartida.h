#pragma once
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida
{
public:
	Esfera esfera;
	Raqueta raqueta1;
	int accion; //1 arriba, 0 nada, -1 abajo
	int fin;// 0 la partida sigue en pie , 1 la partida se ha acabado
};
