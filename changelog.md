# Changelog

## v1.4 - November 30, 2021

**Added:**

- Servidor

- Cliente 



## v1.3 - November 16, 2021

**Added:**

- Bot

- Logger

- Datos de Memoria Compartida

**Changed:**

- Ahora el juego se termina cuando uno de los dos jugadores consigue 3 puntos.



## v1.2 - November 2, 2021

**Added:**

- README

**Changed:**

- Se ha añadido el movimiento a la esfera.

- Se ha añadido el movimiento a las raquetas.

- Se ha añadido la funcionalidad especial a la esfera. 



## v1.1 - October 19, 2021

Se ha realizado el fork del repositorio.

**Added:**

- Chagelog

**Changed:**

- Se ha realizado la modificación de la cabecera de los ficheros fuente.
