# README

## Practicas del laboratorio de Sistemas Informáticos Industriales 2021/2022

## Autor: Ernesto Liébana Pérez  Matrícula: 53389


## Juego de Tenis

**Controles:**

- Jugador 1:

Tecla W: Arriba
 
Tecla S: Abajo
 
 
- Jugador 2: 
 
Tecla L: Arriba
  
Tecla O: Abajo
 
**Funcionalidad extra añadida: El tamaño de la pelota se encoge y vuelve a su tamaño normal a medida que pasa el tiempo**
